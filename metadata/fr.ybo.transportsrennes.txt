AntiFeatures:UpstreamNonFree
Categories:Navigation
# See COPYING.txt
License:GPLv3
Web Site:
Source Code:https://github.com/ybonnel/TransportsRennes/tree/noGoogleMap
Issue Tracker:https://github.com/ybonnel/TransportsRennes/issues

Auto Name:Transports Rennes
#Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=ybonnel%40gmail.com&item_name=Transports%20Rennes%20application%20donation&currency_code=EUR
Summary:Transport info for France
Description:
Transports Rennes provides quick access to information about public
transportation of the city of Rennes, France and its sprawl.
Information comes from the open data initiative by Keolis Rennes, the
company running Rennes’ public transportation system.

Features:
* Bus and metro schedule (widget available for even faster access)
* Bus/metro itinerary computation
* Nearby bus stops and “LE vélo STAR” bikesharing stations
* Number of available bikes in “LE vélo STAR” stations
* Favorite bus stops, metro and bikesharing stations management
* Available parking space in park-and-ride facilities
* Disruption of service alerts and Twitter stream (@starbusmetro)

Google Maps are disabled in these builds. You can see the location of a
situation with a cartographic app such as [[com.robert.maps]] or
[[net.osmand.plus]] though. Proprietary libraries have been removed.
.

Repo Type:srclib
Repo:TransportsRennes

Build:3.4.3,343
    commit=TR_noGoogleMap_3.4.3
    subdir=TransportsRennes
    update=.,../TransportsCommun
    prebuild=rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.4.4,344
    commit=TR_noGoogleMap_3.4.4
    subdir=TransportsRennes
    update=.,../TransportsCommun
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.4.5,345
    commit=TR_noGoogleMap_3.4.5
    subdir=TransportsRennes
    update=.,../TransportsCommun
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.4.7,347
    commit=TR_noGoogleMap_3.4.7
    subdir=TransportsRennes
    update=.,../TransportsCommun
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.4.8,348
    commit=TR_noGoogleMap_3.4.8
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.4.9,349
    commit=TR_noGoogleMap_3.4.9
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.5.1,351
    commit=TR_noGoogleMap_3.5.1
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.5.2,352
    commit=TR_noGoogleMap_3.5.2
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.5.3,353
    commit=TR_noGoogleMap_3.5.3
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.5.4,354
    commit=TR_noGoogleMap_3.5.4
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.5.5,355
    commit=TR_noGoogleMap_3.5.5
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.5.6,356
    commit=TR_noGoogleMap_3.5.6
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.5.7,357
    commit=TR_noGoogleMap_3.5.7
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.5.9,359
    commit=TR_noGoogleMap_3.5.9
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.6.0,360
    commit=TR_noGoogleMap_3.6.0
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Build:3.6.1,361
    commit=TR_noGoogleMap_3.6.1
    subdir=TransportsRennes
    gradle=yes
    prebuild=sed -i '8s/true/false/g' res/xml/preferences.xml && \
        rm ../TransportsBordeaux/libs/GoogleAdMob*.jar

Auto Update Mode:None
Update Check Mode:RepoManifest/noGoogleMap
Current Version:3.6.1
Current Version Code:361

