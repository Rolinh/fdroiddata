Categories:Office
License:MIT
Web Site:https://github.com/xgouchet/Ted
Source Code:https://github.com/xgouchet/Ted
Issue Tracker:https://github.com/xgouchet/Ted/issues

Auto Name:Ted
Summary:Lightweight text editor
Description:
Ted is a lightweight text editor, meant as a Notepad application,
and not meant to edit big files. You can create new text file, open existing files and of
 course save them. You can also display line numbers and open recent files. You can also
 search for text inside the opened file.
.

Repo Type:git
Repo:https://github.com/xgouchet/Ted.git

Build:Release 1.3,13
    commit=d4816fd
    subdir=eclipse-project
    prebuild=sed -i '/OneCloudApp/d' project.properties && \
        rm -rf ../release

Build:Release 1.7,17
    commit=412f7900c3
    subdir=Ted
    submodules=yes
    target=android-15
    update=.,../AndroidLib
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1../AndroidLib@' project.properties

Build:Release 1.8.1,19
    commit=4eeb93d6f
    subdir=Ted
    submodules=yes
    target=android-15
    update=.,../AndroidLib
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1../AndroidLib@' project.properties

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:Release 1.8.1
Current Version Code:19

